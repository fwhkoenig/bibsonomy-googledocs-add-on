# BibSonomy GoogleDocs Add-on

The BibSonomy GoogleDocs Add-on helps inserting citation sources to a document or paper in a convenient way. The Add-on uses the data available on BibSonomy to access your bookmarks. It also features searching with automatic completion to make it easier to find the required citation source.