/* 
 * To include this script into the GoogleDocs script editor you have to
 * add these first two lines to the top and the third line to the end of this file:
 * 
 * 1. <?!= include("TreeIndexNode.js"); ?>
 * 2. <script>
 * 3. </script>
 */

/**
 * This function creates the index of the given documents
 * 
 * @param {Object}
 *            userPosts - The JSON.posts.post object from the BibSonomy API.
 */
TreeIndex.prototype.createIndex = function(userPosts) {
	var index = {};
	for (var i = 0, keys = Object.keys(userPosts), size = keys.length; i < size; i++) {
		var post = userPosts[i];
		var interhash = post.bibtex.interhash;
		var title = post.bibtex.title;
		var words = title.match(/\w+/g);
		words = this.filterStopWords(words);
		for (var j = 0; j < words.length; j++) {
			var word = words[j];
			word = word.toLowerCase();
			this.autoCompleteTags.push(word);
			var previousContainer = index;
			for (var w = 0; w < word.length; w++) {
				var character = word[w];
				var currentNode = previousContainer[character.toString()];
				if (currentNode === undefined) {
					currentNode = new TreeIndexNode(this);
					previousContainer[character.toString()] = currentNode;
				}
				if (w < word.length - 1) {
					currentNode.addChildDocument(interhash);
					previousContainer = currentNode;
				} else {
					currentNode.addDocument(interhash);
				}
			}
		}
	}
	this.index = index;
};

/**
 * This function returns all words in the index.
 * 
 * @returns {Array} All words in the index
 */
TreeIndex.prototype.getAutoCompleteTags = function() {
	return this.autoCompleteTags;
};

/**
 * This function returns all documents in this index.
 * 
 * @returns {Array} All documents in the index.
 */
TreeIndex.prototype.getAllDocuments = function() {
	return this.allDocuments;
};

/**
 * This function adds a document interhash into the index.
 * 
 * @param {String}
 *            interhash - The interhash to add to the index.
 * @returns {Boolean} True if the document was added, false if it was already in
 *          the index.
 */
TreeIndex.prototype.addDocument = function(interhash) {
	if (this.allDocuments.indexOf(interhash) != -1)
		return false;
	this.allDocuments.push(interhash);
	return true;
};

/**
 * This function removes stop words from the given word list.
 * 
 * @param words
 * @returns filtered list
 */
TreeIndex.prototype.filterStopWords = function(words) {
	var stopWordList = this.stopWordList;
	return words.filter(function(e, node) {
		if (stopWordList.indexOf(e) == -1)
			return true;
	});
};

/**
 * Setter for the stop word list.
 * 
 * @param {Array}
 *            stopWordList - The new stop word list.
 */
TreeIndex.prototype.setStopWordList = function(stopWordList) {
	this.stopWordList = stopWordList;
};

/**
 * Getter for the stop word list.
 * 
 * @returns {Array} The stop word list.
 */
TreeIndex.prototype.getStopWordList = function() {
	return this.stopWordList;
};

/**
 * This function retrieves a list of matching documents for a list of search
 * terms.
 * 
 * @param {Array}
 *            searchTerms - List of search terms.
 * @returns {Array} Array of matching document interhashes.
 */
TreeIndex.prototype.retrieveMatchingDocuments = function(searchTerms) {
	var matchingDocuments = undefined;
	searchTerms = this.filterStopWords(searchTerms);
	for (var s = 0; s < searchTerms.length; s++) {
		var term = searchTerms[s];
		term = term.toLowerCase();
		var matchingDocumentsForTerm = this
				.retrieveMatchingDocumentsForTerm(term);
		if (matchingDocuments === undefined)
			matchingDocuments = matchingDocumentsForTerm;
		else
			matchingDocuments = this.intersectArrays(matchingDocuments,
					matchingDocumentsForTerm);
	}
	return matchingDocuments;
};

/**
 * This function retrieves a list of matching documents for one search term.
 * 
 * @param {String}
 *            term - Term to search for.
 * @returns {Array} Array of matching document interhashes.
 */
TreeIndex.prototype.retrieveMatchingDocumentsForTerm = function(term) {
	var index = this.index;
	var currentContainer = index;
	for (var c = 0; c < term.length; c++) {
		var character = term[c];
		var nextContainer = currentContainer[character.toString()];
		if (nextContainer === undefined)
			return [];
		if (c < term.length - 1) {
			currentContainer = nextContainer;
		} else {
			return nextContainer.getDocuments();
		}
		;
	}
	;
};

/**
 * This function intersects two arrays.
 * 
 * @param {Array}
 *            a - The first array.
 * @param {Array}
 *            b - The second array.
 * @returns {Array} The intersected array.
 */
TreeIndex.prototype.intersectArrays = function intersect(a, b) {
	return a.filter(function(e) {
		if (b.indexOf(e) !== -1)
			return true;
	});
};

/**
 * Constructor for TreeIndex.
 * 
 * @returns {TreeIndex} The new TreeIndex object.
 */
function TreeIndex() {
	this.searchForPartialWords = false;
	this.stopWordList = [];
	this.allDocuments = [];
	this.index = {};
	this.autoCompleteTags = []
};