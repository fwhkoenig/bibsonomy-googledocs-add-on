var map = {};
var citeCounter = 0;
var userPosts;

// Stores the returned JSON from BibSonomy
var currentlyLoadedCitations = {};

// Used to Store the documents that are cited within the document. Will also be
// stored as DocumentProperty.
var citesInDocument = {};

/**
 * Loads the citationData from the documentProperties Storage into the variable
 * citesInDocument.
 */
function loadStoredCitationData() {

	// The document properties store can save a total of 500kb.
	// Each key-value property may use up to 9kb.
	// Because of that we'll store each used citation as a seperate property.

	var documentProperties = PropertiesService.getDocumentProperties();
	var storedData = documentProperties.getProperties();
	citesInDocument = {};

	for ( var key in storedData) {
		Logger.log(key, storedData[key]);
		if (storedData[key] != 'undefined') {
			citesInDocument[key] = JSON.parse(storedData[key]);
		} else {
			Logger.log("Deleted property with undefined value: " + key);
			documentProperties.deleteProperty(key);
		}
	}
}

function renameRanges() {
	Locking.prototype.lock();

	var AllRanges = DocumentApp.getActiveDocument().getNamedRanges();
	if (!AllRanges) {
		DocumentApp.getUi().alert('Cannot find ranges in the document.');
		return;
	}
	var mapCounter = map
	var citeCounter = 0;
	for (var i = 0; i < AllRanges.length; ++i) {
		var selectedElement = AllRanges[i];
		var selEleName = selectedElement.getName();

		// Checks if the element name starts with BibSonomyCite-, if not, it
		// will be skipped
		if (selEleName.lastIndexOf("BibSonomyCite-", 0) === 0) {
			if (selEleName in map) {
				// Lösche alten Text und setze neuen ein
				var text = selectedElement.getRange().getRangeElements()[0]
						.getElement().editAsText();
				var pos = selectedElement.getRange().getRangeElements()[0]
						.getStartOffset();
				var end = selectedElement.getRange().getRangeElements()[0]
						.getEndOffsetInclusive();
				text.insertText(end, map[selEleName]);
				text.deleteText(pos, end - 1);
				text.deleteText(end, end);

			} else { // Erstmalig gefunden
				citeCounter++;
				map[selEleName] = "[" + citeCounter + "]";

				// Lösche alten Text und setze neuen ein
				var text = selectedElement.getRange().getRangeElements()[0]
						.getElement().editAsText();
				var pos = selectedElement.getRange().getRangeElements()[0]
						.getStartOffset();
				var end = selectedElement.getRange().getRangeElements()[0]
						.getEndOffsetInclusive();
				text.insertText(end, map[selEleName]);
				text.deleteText(pos, end - 1);
				text.deleteText(end, end);
			}
		}
	}
	rebuildMap();
	createReferences();
	Locking.prototype.unlock();
}

/**
 * This function rebuilds our local map, to make sure we're working on the
 * latest data. Additionally, this will also clean up the documentProperties
 * storage from unused data to free space.
 */
function rebuildMap() {
	loadStoredCitationData();
	var AllRanges = DocumentApp.getActiveDocument().getNamedRanges();
	if (!AllRanges) {
		DocumentApp.getUi().alert('Cannot find ranges in the document.');
		return;
	}
	map = {};
	citeCounter = 0;
	for (var i = 0; i < AllRanges.length; ++i) {
		var selectedElement = AllRanges[i];

		var selEleName = selectedElement.getName();
		if (selEleName.lastIndexOf("BibSonomyCite-", 0) === 0) {

			var text = selectedElement.getRange().getRangeElements()[0]
					.getElement().editAsText().getText();
			var pos = selectedElement.getRange().getRangeElements()[0]
					.getStartOffset();
			var end = selectedElement.getRange().getRangeElements()[0]
					.getEndOffsetInclusive();
			if (!(selEleName in map)) {
				citeCounter++;
			}
			map[selEleName] = text.substring(pos, end + 1);
		}
	}

	// Remove unused citations from the storage.
	var documentProperties = PropertiesService.getDocumentProperties();
	for ( var key in citesInDocument) {
		if (!(key in map) && key != "lock") {
			documentProperties.deleteProperty(key);
		} else {
		}
	}
	return citeCounter;
}

function getCiteNumber(hashId) {
	rebuildMap();

	if (hashId in map) {
		return map[hashId];
	} else {
		citeCounter++;
		map[hashId] = "[" + citeCounter + "]";
		return map[hashId];
	}
}

function getHashForCite(citeNumber) {
	for ( var key in map) {
		if (map.hasOwnProperty(key)) {
			if (map[key] == "[" + citeNumber + "]") {
				return key;
			}
		}
	}
	return undefined;
}

function insertBetter(position, textToInsert, isBold, isItalic) {
	var pos = position.getText().length;
	position.insertText(pos, textToInsert);
	position.setBold(pos, pos + textToInsert.length - 1, isBold);
	position.setItalic(pos, pos + textToInsert.length - 1, isItalic);
}

function insertReference(position, publication, referencePosition) {
	var row = position.appendTableRow();
	var cell = row.appendTableCell("[" + referencePosition + "]");
	cell.setWidth(30);
	cell.setPaddingBottom(0);
	cell.setPaddingTop(0);
	var cell2 = row.appendTableCell("");
	cell2.setPaddingBottom(0);
	cell2.setPaddingTop(0);

	position = cell2.editAsText();

	// Python Script https://bitbucket.org/bibsonomy/bibsonomy-python/overview
	// Copyright 2014 Robert Jäschke
	insertBetter(position, publication["title"] + '\r', true, false);
	if (publication["author"] != undefined) {
		insertBetter(position, publication["author"], false, true);
	} else if (publication["editor"] != undefined) {
		insertBetter(position, publication["editor"], false, true);
	}

	// Hyperlinks are not included
	if (publication["entrytype"] == "article") {
		if (publication["journal"] != undefined) {
			insertBetter(position, " " + publication["journal"], false, false);
		}
		if (publication["volume"] != undefined) {
			insertBetter(position, " " + publication["volume"] + " ", false,
					false);
		}
		if (publication["number"] != undefined) {
			insertBetter(position, " (" + publication["number"] + ") ", false,
					false);
		}
		if (publication["pages"] != undefined) {
			insertBetter(position, " " + publication["pages"] + " ", false,
					false);
		}
	} else if (publication["entrytype"] == "inproceedings"
			|| publication["entry_type"] == "incollection"
			|| publication["entry_type"] == "inbook") {
		if (publication["booktitle"] != undefined) {
			insertBetter(position, ", " + publication["booktitle"], false,
					false);
			if (publication["pages"] != undefined) {
				insertBetter(position, ", " + publication["pages"], false,
						false);
			}
		}
		if (publication["publisher"] != undefined) {
			if (publication["booktitle"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["publisher"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		} else if (publication["organization"] != undefined) {
			if (publication["booktitle"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["organization"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		}
	} else if (publication["entrytype"] == "book"
			|| publication["entry_type"] == "booklet"
			|| publication["entry_type"] == "proceedings") {
		if (publication["publisher"] != undefined) {
			insertBetter(position, publication["publisher"], false, false);
		}
	} else if (publication["entrytype"] == "mastersthesis"
			|| publication["entry_type"] == "phdthesis") {
		if (publication["school"] != undefined) {
			insertBetter(position, publication["school"], false, false);
			if (publication["address"] != undefined) {
				insertBetter(position, ", " + publication["address"], false,
						false);
			}
		}
	} else if (publication["entrytype"] == "techreport") {
		if (publication["type"] != undefined) {
			insertBetter(position, publication["type"], false, false);
		} else {
			insertBetter(position, "Technical Report", false, false);
		}
		if (publication["number"] != undefined) {
			insertBetter(position, " " + publication["number"], false, false);
		}
		if (publication["institution"] != undefined) {
			insertBetter(position, ", " + publication["institution"], false,
					false);
		}
	} else if (publication["entrytype"] == "manual") {
		if (publication["organization"] != undefined) {
			insertBetter(position, publication["organization"], false, false);
		}
		if (publication["address"] != undefined) {
			if (publication["organization"] != undefined) {
				insertBetter(position, ", ", false, false);
			}
			insertBetter(position, publication["address"], false, false);
		}
	}
	insertBetter(position, " (" + publication["year"] + ") ", false, false);

	if (publication["abstract"] != undefined) {
		insertBetter(position, "  <div class='abstract'>"
				+ post.resource.abstract + "</div>", false, false);
	}

}

/**
 * Inserts the citation at the current cursor location in boldface.
 */
function insertAtCursor(hash, json) {
	var locked = getLockStatus();
	if (locked == 'true') {
		DocumentApp
				.getUi()
				.alert(
						"Another user is currently working on the citations, please wait a few seconds and try again.");
		return false;
	} else {

		Locking.prototype.lock();

		var cursor = DocumentApp.getActiveDocument().getCursor();
		if (cursor) {
			// Attempt to insert text at the cursor position. If insertion
			// returns null,
			// then the cursor's containing element doesn't allow text
			// insertions.
			cursor.insertText(" ");
			var element = cursor.insertText(getCiteNumber('BibSonomyCite-'
					+ hash));

			if (element) {
				var rangeBuilder = DocumentApp.getActiveDocument().newRange();
				rangeBuilder.addElement(element, 0,
						element.getText().length - 1);
				// Maximal noch 200 Zeichen. Insgesamt 248 Zeichen
				DocumentApp.getActiveDocument().addNamedRange(
						'BibSonomyCite-' + hash, rangeBuilder.build());

				var documentProperties = PropertiesService
						.getDocumentProperties();
				documentProperties.setProperty('BibSonomyCite-' + hash, JSON
						.stringify(filterBibtexData(json)));
			} else {
				DocumentApp.getUi().alert(
						'Cannot insert text at this cursor location.');
			}
		} else {
			DocumentApp.getUi().alert('Cannot find a cursor in the document.');
		}
		return true;
	}
}

function createReferences(onlyCreate) {
	var citationSource = DocumentApp.getActiveDocument().getNamedRanges(
			'BibSonomySourceCitation')
	var citationNamedRange;
	if (citationSource.length == 0) {
		var body = DocumentApp.getActiveDocument().getBody();

		// Build a table from the array.
		body.appendParagraph("Quellenverzeichnis")
		var citationTable = body.appendTable();
		citationTable.setBorderWidth(0);

		var rangeBuilder = DocumentApp.getActiveDocument().newRange();
		rangeBuilder.addElement(citationTable);
		citationNamedRange = DocumentApp.getActiveDocument().addNamedRange(
				'BibSonomySourceCitation', rangeBuilder.build());
	} else {
		citationNamedRange = citationSource[0];
		if (citationNamedRange.getRange().getRangeElements().length == 0) {
			citationNamedRange.remove();
			createReferences(onlyCreate);
			return 0;
		}
	}

	if (!onlyCreate) {
		citationNamedRange.getRange().getRangeElements()[0].getElement()
				.asTable().clear();
		for (var i = 1; i <= citeCounter; i++) {
			insertReference(citationNamedRange.getRange().getRangeElements()[0]
					.getElement().asTable(),
					citesInDocument[getHashForCite(i)], i);
		}
	}
}