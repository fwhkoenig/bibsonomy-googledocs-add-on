function saveSettings(userName, apiKey) {
	var userProperties = PropertiesService.getUserProperties();
	userProperties.setProperties({
		'BibSonomy-userName' : userName,
		'BibSonomy-API-Key' : apiKey
	});
}

function getUserName() {
	var userProperties = PropertiesService.getUserProperties();
	var userName = userProperties.getProperty('BibSonomy-userName');
	if (userName === undefined) {
		userName = "";
	}
	return userName;
}

function getApiKey() {
	var userProperties = PropertiesService.getUserProperties();
	return userProperties.getProperty('BibSonomy-API-Key');
}

function getLockStatus() {
	var documentProperties = PropertiesService.getDocumentProperties();
	return documentProperties.getProperty('lock');
}