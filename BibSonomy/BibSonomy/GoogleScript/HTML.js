/**
 * Opens a sidebar in the document containing the add-on's user interface.
 */
function showSidebar() {
	var ui = doGet("Sidebar").setTitle("Cite from BibSonomy");
	DocumentApp.getUi().showSidebar(ui);
}

function showSettingsDialog() {
	var html = HtmlService.createHtmlOutputFromFile('options');
	html.setWidth(200);
	html.setHeight(150);
	DocumentApp.getUi().showModalDialog(html, 'Settings');
}

/**
 * Returns the content of the selected file for import into another file.
 */
function include(filename) {
	return HtmlService.createTemplateFromFile(filename).evaluate()
			.setSandboxMode(HtmlService.SandboxMode.IFRAME).getContent();
}

/**
 * Returns a HTMLTemplate from the file
 */
function doGet(filename) {
	return HtmlService.createTemplateFromFile(filename).evaluate();
}
