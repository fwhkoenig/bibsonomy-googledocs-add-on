/**
 * Fetches Bibtex data from the BibSonomy API. This fetches all publications of
 * a user.
 * 
 * @param {String}
 *            userName - The user name to fetch data from
 * @param {Integer}
 *            step - The number of posts to fetch simultaneously (Max 1000)
 * @param {Object}
 *            options - The login data for the API.
 * @returns {String} JSON string with all publication data.
 */
BibSonomyFetch.prototype.fetch = function(userName, step, options) {
	var userPosts = {};
	var offset = 0;
	var url = 'http://www.bibsonomy.org/api/users/' + userName
			+ '/posts?resourcetype=bibtex&start=' + offset + '&end='
			+ (offset + step);
	var nameFormatter = new NameFormatter();
	while (url !== undefined) {
		var response = UrlFetchApp.fetch(url + '&format=json', options);
		var parsedJSON = JSON.parse(response.getContentText());

		var newUserPosts = parsedJSON.posts.post;
		for ( var attrname in newUserPosts) {
			var authors = newUserPosts[attrname].bibtex.author;
			var id = (parseInt(attrname) + offset).toString();
			userPosts[id] = newUserPosts[attrname];
			if (authors !== undefined)
				userPosts[id].bibtex.author = nameFormatter
						.formatAuthors(authors);
		}

		url = parsedJSON.posts.next;
		offset = offset + step;
	}
	var userPostsString = JSON.stringify(userPosts);
	return userPostsString;
}

/**
 * Creates an options object for fetching data from the BibSonomy API using API
 * key.
 * 
 * @param {String}
 *            userName - The user name to fetch data from.
 * @param {String}
 *            apiKey - The API key for the user.
 * @returns {Object} Options for fetching data from the API.
 */
BibSonomyFetch.prototype.getApiOptions = function(userName, apiKey) {
	var headers = {
		"muteHttpExceptions" : true,
		"User-Agent" : "GoogleAppsTestScript",
		"Authorization" : "Basic "
				+ Utilities.base64Encode(userName + ":" + apiKey)
	};
	var options = {
		'headers' : headers
	};
	return options;
}

/**
 * Creates an options object for fetching data from the BibSonomy API using
 * OAuth.
 * 
 * @returns {Object} Options for fetching data from the API.
 */
BibSonomyFetch.prototype.getOAuthOptions = function() {
	var consumerKey = 'googledoc';
	var consumerSecret = 'ohx4Dulu eM3zooCh';

	var oauthConfig = UrlFetchApp.addOAuthService('bibsonom5');
	oauthConfig.setAccessTokenUrl('http://www.bibsonomy.org/oauth/accessToken');
	oauthConfig
			.setRequestTokenUrl('http://www.bibsonomy.org/oauth/requestToken');
	oauthConfig.setAuthorizationUrl('http://www.bibsonomy.org/oauth/authorize');
	oauthConfig.setConsumerKey(consumerKey);
	oauthConfig.setConsumerSecret(consumerSecret);

	var options = {
		muteHttpExceptions : true,
		'oAuthServiceName' : 'bibsonom5',
		'oAuthUseToken' : 'always'
	};
	return options;
}

/**
 * Constructor for BibSonomyFetch.
 * 
 * @returns {BibSonomyFetch} The new BibSonomyFetch object.
 */
function BibSonomyFetch() {

}